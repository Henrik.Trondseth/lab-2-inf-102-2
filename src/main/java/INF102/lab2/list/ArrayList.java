//brukt kode fra egen lab fra året før

package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		if(index < 0 || index >= size()){
			throw new IndexOutOfBoundsException();
		}
		
		return (T) elements[index];

	}
	
	@Override
	public void add(int index, T element) {
		if (size() == elements.length) {
			elements = Arrays.copyOf(elements, (size() + 1) * 2);
		}
		if (index == size()) {
			elements[index] = element;
		} else {
			for (int i = index; i < (size() + 1); i++) {
				T item = (T) elements[i];
				elements[i] = element;
				element = item;
			}
		}
		n++;
	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}